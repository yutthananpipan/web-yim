-- --------------------------------------------------------
-- Host:                         172.16.10.243
-- Server version:               10.3.27-MariaDB-1:10.3.27+maria~bionic - mariadb.org binary distribution
-- Server OS:                    debian-linux-gnu
-- HeidiSQL Version:             11.2.0.6213
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


-- Dumping database structure for YIMDB
CREATE DATABASE IF NOT EXISTS `YIMDB` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `YIMDB`;

-- Dumping structure for table YIMDB.auth_assignment
CREATE TABLE IF NOT EXISTS `auth_assignment` (
  `item_name` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `user_id` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` int(11) DEFAULT NULL,
  PRIMARY KEY (`item_name`,`user_id`),
  KEY `idx-auth_assignment-user_id` (`user_id`),
  CONSTRAINT `auth_assignment_ibfk_1` FOREIGN KEY (`item_name`) REFERENCES `auth_item` (`name`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table YIMDB.auth_assignment: ~2 rows (approximately)
DELETE FROM `auth_assignment`;
/*!40000 ALTER TABLE `auth_assignment` DISABLE KEYS */;
INSERT INTO `auth_assignment` (`item_name`, `user_id`, `created_at`) VALUES
	('Admin', '1', 1594122674),
	('USER MANAGER', '6', 1594123899);
/*!40000 ALTER TABLE `auth_assignment` ENABLE KEYS */;

-- Dumping structure for table YIMDB.auth_item
CREATE TABLE IF NOT EXISTS `auth_item` (
  `name` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `type` smallint(6) NOT NULL,
  `description` mediumtext COLLATE utf8_unicode_ci DEFAULT NULL,
  `rule_name` varchar(64) COLLATE utf8_unicode_ci DEFAULT NULL,
  `data` blob DEFAULT NULL,
  `created_at` int(11) DEFAULT NULL,
  `updated_at` int(11) DEFAULT NULL,
  PRIMARY KEY (`name`),
  KEY `rule_name` (`rule_name`),
  KEY `idx-auth_item-type` (`type`),
  CONSTRAINT `auth_item_ibfk_1` FOREIGN KEY (`rule_name`) REFERENCES `auth_rule` (`name`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table YIMDB.auth_item: ~13 rows (approximately)
DELETE FROM `auth_item`;
/*!40000 ALTER TABLE `auth_item` DISABLE KEYS */;
INSERT INTO `auth_item` (`name`, `type`, `description`, `rule_name`, `data`, `created_at`, `updated_at`) VALUES
	('/admin/*', 2, NULL, NULL, NULL, 1594122326, 1594122326),
	('/customer/*', 2, NULL, NULL, NULL, 1594174449, 1594174449),
	('/gii/*', 2, NULL, NULL, NULL, 1594122150, 1594122150),
	('/license/*', 2, NULL, NULL, NULL, 1594174446, 1594174446),
	('/project/*', 2, NULL, NULL, NULL, 1594174458, 1594174458),
	('/user/*', 2, NULL, NULL, NULL, 1594122173, 1594122173),
	('/user/admin/*', 2, NULL, NULL, NULL, 1594122161, 1594122161),
	('Admin', 1, ' สามารถจัดการทุกฟังก์ชัน', NULL, NULL, 1594121381, 1621313956),
	('customer', 2, 'create update delete customer', NULL, NULL, 1594174503, 1594174503),
	('license', 2, 'create update delete', NULL, NULL, 1594122080, 1594174524),
	('manage-user', 2, NULL, NULL, NULL, 1621313698, 1621313822),
	('project', 2, 'create update delete', NULL, NULL, 1594174552, 1594174552),
	('root', 2, 'root', NULL, NULL, 1621313847, 1621313847),
	('USER MANAGER', 1, 'จัดการทุกฟังก์ชันเหมือนผู้ดูแลระบบ ยกเว้นเข้าใช้งานระดับ root', NULL, NULL, 1594121632, 1621313993);
/*!40000 ALTER TABLE `auth_item` ENABLE KEYS */;

-- Dumping structure for table YIMDB.auth_item_child
CREATE TABLE IF NOT EXISTS `auth_item_child` (
  `parent` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `child` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`parent`,`child`),
  KEY `child` (`child`),
  CONSTRAINT `auth_item_child_ibfk_1` FOREIGN KEY (`parent`) REFERENCES `auth_item` (`name`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `auth_item_child_ibfk_2` FOREIGN KEY (`child`) REFERENCES `auth_item` (`name`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table YIMDB.auth_item_child: ~11 rows (approximately)
DELETE FROM `auth_item_child`;
/*!40000 ALTER TABLE `auth_item_child` DISABLE KEYS */;
INSERT INTO `auth_item_child` (`parent`, `child`) VALUES
	('Admin', 'root'),
	('Admin', 'USER MANAGER'),
	('customer', '/customer/*'),
	('license', '/license/*'),
	('manage-user', '/user/*'),
	('manage-user', '/user/admin/*'),
	('project', '/project/*'),
	('root', '/admin/*'),
	('root', '/gii/*'),
	('root', 'manage-user'),
	('USER MANAGER', 'customer'),
	('USER MANAGER', 'license'),
	('USER MANAGER', 'manage-user'),
	('USER MANAGER', 'project');
/*!40000 ALTER TABLE `auth_item_child` ENABLE KEYS */;

-- Dumping structure for table YIMDB.auth_rule
CREATE TABLE IF NOT EXISTS `auth_rule` (
  `name` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `data` blob DEFAULT NULL,
  `created_at` int(11) DEFAULT NULL,
  `updated_at` int(11) DEFAULT NULL,
  PRIMARY KEY (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table YIMDB.auth_rule: ~0 rows (approximately)
DELETE FROM `auth_rule`;
/*!40000 ALTER TABLE `auth_rule` DISABLE KEYS */;
/*!40000 ALTER TABLE `auth_rule` ENABLE KEYS */;

-- Dumping structure for table YIMDB.customer
CREATE TABLE IF NOT EXISTS `customer` (
  `id` varchar(11) COLLATE utf8_unicode_ci NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` int(11) DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_at` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table YIMDB.customer: ~0 rows (approximately)
DELETE FROM `customer`;
/*!40000 ALTER TABLE `customer` DISABLE KEYS */;
INSERT INTO `customer` (`id`, `name`, `created_at`, `created_by`, `updated_at`, `updated_by`) VALUES
	('C0001', 'Royal Forest Department', NULL, NULL, NULL, NULL);
/*!40000 ALTER TABLE `customer` ENABLE KEYS */;

-- Dumping structure for table YIMDB.license
CREATE TABLE IF NOT EXISTS `license` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `auth_key` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `license_user` int(11) DEFAULT 0,
  `confirmed_at` int(11) DEFAULT NULL,
  `blocked_at` int(11) DEFAULT NULL,
  `created_at` int(11) DEFAULT NULL,
  `updated_at` int(11) DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `expire_date` int(11) DEFAULT NULL,
  `cust_number` varchar(11) COLLATE utf8_unicode_ci DEFAULT NULL,
  `project_number` varchar(11) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  KEY `FK_license_customer` (`cust_number`),
  KEY `FK_license_project` (`project_number`),
  CONSTRAINT `FK_license_customer` FOREIGN KEY (`cust_number`) REFERENCES `customer` (`id`) ON UPDATE CASCADE,
  CONSTRAINT `FK_license_project` FOREIGN KEY (`project_number`) REFERENCES `project` (`number`) ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table YIMDB.license: ~2 rows (approximately)
DELETE FROM `license`;
/*!40000 ALTER TABLE `license` DISABLE KEYS */;
INSERT INTO `license` (`id`, `auth_key`, `license_user`, `confirmed_at`, `blocked_at`, `created_at`, `updated_at`, `created_by`, `updated_by`, `expire_date`, `cust_number`, `project_number`) VALUES
	(2, 'gE9EIHosUyRTEZHJKSye2FqDbjaq0Sx_', 100, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'C0001', 'P0001'),
	(3, 'lw_pKP6QElzPpDs8zEuHmjO2xU5qKVHb', 10, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'C0001', 'P0001');
/*!40000 ALTER TABLE `license` ENABLE KEYS */;

-- Dumping structure for table YIMDB.migration
CREATE TABLE IF NOT EXISTS `migration` (
  `version` varchar(180) NOT NULL,
  `apply_time` int(11) DEFAULT NULL,
  PRIMARY KEY (`version`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Dumping data for table YIMDB.migration: ~17 rows (approximately)
DELETE FROM `migration`;
/*!40000 ALTER TABLE `migration` DISABLE KEYS */;
INSERT INTO `migration` (`version`, `apply_time`) VALUES
	('m000000_000000_base', 1594116883),
	('m140209_132017_init', 1594116922),
	('m140403_174025_create_account_table', 1594116923),
	('m140504_113157_update_tables', 1594116923),
	('m140504_130429_create_token_table', 1594116923),
	('m140506_102106_rbac_init', 1594117652),
	('m140602_111327_create_menu_table', 1621312439),
	('m140830_171933_fix_ip_field', 1594116923),
	('m140830_172703_change_account_table_name', 1594116923),
	('m141222_110026_update_ip_field', 1594116923),
	('m141222_135246_alter_username_length', 1594116923),
	('m150614_103145_update_social_account_table', 1594116923),
	('m150623_212711_fix_username_notnull', 1594116923),
	('m151218_234654_add_timezone_to_profile', 1594116924),
	('m160312_050000_create_user', 1621312439),
	('m160929_103127_add_last_login_at_to_user_table', 1594116924),
	('m170907_052038_rbac_add_index_on_auth_assignment_user_id', 1594117652),
	('m180523_151638_rbac_updates_indexes_without_prefix', 1594117652),
	('m200409_110543_rbac_update_mssql_trigger', 1594117652);
/*!40000 ALTER TABLE `migration` ENABLE KEYS */;

-- Dumping structure for table YIMDB.profile
CREATE TABLE IF NOT EXISTS `profile` (
  `user_id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `public_email` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `gravatar_email` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `gravatar_id` varchar(32) COLLATE utf8_unicode_ci DEFAULT NULL,
  `location` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `website` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `bio` text COLLATE utf8_unicode_ci DEFAULT NULL,
  `timezone` varchar(40) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`user_id`),
  CONSTRAINT `fk_user_profile` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table YIMDB.profile: ~2 rows (approximately)
DELETE FROM `profile`;
/*!40000 ALTER TABLE `profile` DISABLE KEYS */;
INSERT INTO `profile` (`user_id`, `name`, `public_email`, `gravatar_email`, `gravatar_id`, `location`, `website`, `bio`, `timezone`) VALUES
	(1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(6, 'manager', '', '', 'd41d8cd98f00b204e9800998ecf8427e', '', '', '', NULL);
/*!40000 ALTER TABLE `profile` ENABLE KEYS */;

-- Dumping structure for table YIMDB.project
CREATE TABLE IF NOT EXISTS `project` (
  `number` varchar(11) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  `version` varchar(11) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  PRIMARY KEY (`number`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table YIMDB.project: ~0 rows (approximately)
DELETE FROM `project`;
/*!40000 ALTER TABLE `project` DISABLE KEYS */;
INSERT INTO `project` (`number`, `name`, `version`) VALUES
	('P0001', 'Forest Resources Survering System', '1.0.1');
/*!40000 ALTER TABLE `project` ENABLE KEYS */;

-- Dumping structure for table YIMDB.social_account
CREATE TABLE IF NOT EXISTS `social_account` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `provider` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `client_id` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `data` text COLLATE utf8_unicode_ci DEFAULT NULL,
  `code` varchar(32) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` int(11) DEFAULT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `username` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `account_unique` (`provider`,`client_id`),
  UNIQUE KEY `account_unique_code` (`code`),
  KEY `fk_user_account` (`user_id`),
  CONSTRAINT `fk_user_account` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table YIMDB.social_account: ~0 rows (approximately)
DELETE FROM `social_account`;
/*!40000 ALTER TABLE `social_account` DISABLE KEYS */;
/*!40000 ALTER TABLE `social_account` ENABLE KEYS */;

-- Dumping structure for table YIMDB.token
CREATE TABLE IF NOT EXISTS `token` (
  `user_id` int(11) NOT NULL,
  `code` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` int(11) NOT NULL,
  `type` smallint(6) NOT NULL,
  UNIQUE KEY `token_unique` (`user_id`,`code`,`type`),
  CONSTRAINT `fk_user_token` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table YIMDB.token: ~0 rows (approximately)
DELETE FROM `token`;
/*!40000 ALTER TABLE `token` DISABLE KEYS */;
/*!40000 ALTER TABLE `token` ENABLE KEYS */;

-- Dumping structure for table YIMDB.user
CREATE TABLE IF NOT EXISTS `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password_hash` varchar(60) COLLATE utf8_unicode_ci NOT NULL,
  `auth_key` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `confirmed_at` int(11) DEFAULT NULL,
  `unconfirmed_email` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `blocked_at` int(11) DEFAULT NULL,
  `registration_ip` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` int(11) NOT NULL,
  `updated_at` int(11) NOT NULL,
  `flags` int(11) NOT NULL DEFAULT 0,
  `last_login_at` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `user_unique_username` (`username`),
  UNIQUE KEY `user_unique_email` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table YIMDB.user: ~2 rows (approximately)
DELETE FROM `user`;
/*!40000 ALTER TABLE `user` DISABLE KEYS */;
INSERT INTO `user` (`id`, `username`, `email`, `password_hash`, `auth_key`, `confirmed_at`, `unconfirmed_email`, `blocked_at`, `registration_ip`, `created_at`, `updated_at`, `flags`, `last_login_at`) VALUES
	(1, 'admin', 'admin@gmail.com', '$2y$12$f1CPm8wuRFBGdHtelLNTVe5LOCRGLDWj.8pGjaMbfxO8LaexWVqTK', 'k9-ysOl7mKIMQR76DWh-WkC9fWmeSWJp', 1594117060, NULL, NULL, '::1', 1594116981, 1594116981, 0, 1621312201),
	(6, 'manager', 'manager@gmail.com', '$2y$12$xVKjRdte/kH2IThjgJOB7.9qTMdIhi1P5tnRfKZJ0BobeLDxBQu3q', 'lw_pKP6QElzPpDs8zEuHmjO2xU5qKVHb', 1594123656, NULL, NULL, '::1', 1594123656, 1594123656, 0, NULL);
/*!40000 ALTER TABLE `user` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IFNULL(@OLD_FOREIGN_KEY_CHECKS, 1) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40111 SET SQL_NOTES=IFNULL(@OLD_SQL_NOTES, 1) */;
