<?php

namespace common\models;

use Yii;
use yii\behaviors\BlameableBehavior;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "{{%message}}".
 *
 * @property string $id
 * @property string|null $message
 * @property int|null $created_at
 * @property int|null $created_by
 * @property int|null $cg_id
 * @property int|null $user_id
 * @property string|null $type STICKER, VOICE, IMAGE, VIDEO, TEXT
 *
 * @property Chatgroup $cg
 */
class Message extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%message}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['message'], 'string'],
            [['created_at', 'created_by', 'cg_id', 'user_id'], 'integer'],
            [['type'], 'string', 'max' => 50],
            [['id'], 'string', 'max' => 17],
            [['cg_id'], 'exist', 'skipOnError' => true, 'targetClass' => Chatgroup::className(), 'targetAttribute' => ['cg_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'message' => 'Message',
            'created_at' => 'Created At',
            'created_by' => 'Created By',
            'cg_id' => 'Cg ID',
            'user_id' => 'User ID',
            'type' => 'STICKER, VOICE, IMAGE, VIDEO, TEXT',
        ];
    }

    /**
     * Gets query for [[Cg]].
     *
     * @return \yii\db\ActiveQuery|ChatgroupQuery
     */
    public function getCg()
    {
        return $this->hasOne(Chatgroup::className(), ['id' => 'cg_id']);
    }

    /**
     * {@inheritdoc}
     * @return MessageQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new MessageQuery(get_called_class());
    }
}
