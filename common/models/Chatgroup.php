<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "{{%chatgroup}}".
 *
 * @property int $id
 * @property string $name
 * @property int|null $created_at
 * @property int|null $created_by
 * @property int|null $updated_at
 * @property int|null $updated_by
 * @property int $user_id
 * @property int|null $last_message
 * @property string|null $type INDIVIDUAL, GROUP
 *
 * @property Conversation[] $conversations
 * @property User[] $users
 * @property Message[] $messages
 */
class Chatgroup extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%chatgroup}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['created_at', 'created_by', 'updated_at', 'updated_by'], 'integer'],
            [['name'], 'string', 'max' => 255],
            [['type'], 'string', 'max' => 50],
            [['last_message'], 'string', 'max' => 50],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'created_at' => 'Created At',
            'created_by' => 'Created By',
            'updated_at' => 'Updated At',
            'updated_by' => 'Updated By',
            'last_message' => 'Last Message',
            'type' => 'INDIVIDUAL, GROUP',
        ];
    }

    /**
     * Gets query for [[Conversations]].
     *
     * @return \yii\db\ActiveQuery|ConversationQuery
     */
    public function getConversations()
    {
        return $this->hasMany(Conversation::className(), ['chatgroup_id' => 'id']);
    }

    /**
     * Gets query for [[Users]].
     *
     * @return \yii\db\ActiveQuery|yii\db\ActiveQuery
     */
    public function getUsers()
    {
        return $this->hasMany(User::className(), ['id' => 'user_id'])->viaTable('{{%conversation}}', ['chatgroup_id' => 'id']);
    }

    /**
     * Gets query for [[Messages]].
     *
     * @return \yii\db\ActiveQuery|MessageQuery
     */
    public function getMessages()
    {
        return $this->hasMany(Message::className(), ['cg_id' => 'id']);
    }

    /**
     * {@inheritdoc}
     * @return ChatgroupQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new ChatgroupQuery(get_called_class());
    }
}
