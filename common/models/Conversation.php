<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "{{%conversation}}".
 *
 * @property int|null $user_id
 * @property int|null $chatgroup_id
 *
 * @property Chatgroup $chatgroup
 * @property User $user
 */
class Conversation extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%conversation}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['user_id', 'chatgroup_id'], 'integer'],
            [['user_id', 'chatgroup_id'], 'unique', 'targetAttribute' => ['user_id', 'chatgroup_id']],
            [['chatgroup_id'], 'exist', 'skipOnError' => true, 'targetClass' => Chatgroup::className(), 'targetAttribute' => ['chatgroup_id' => 'id']],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['user_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'user_id' => 'User ID',
            'chatgroup_id' => 'Chatgroup ID',
        ];
    }

    /**
     * Gets query for [[Chatgroup]].
     *
     * @return \yii\db\ActiveQuery|ChatgroupQuery
     */
    public function getChatgroup()
    {
        return $this->hasOne(Chatgroup::className(), ['id' => 'chatgroup_id']);
    }

    /**
     * Gets query for [[User]].
     *
     * @return \yii\db\ActiveQuery|yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

    /**
     * {@inheritdoc}
     * @return ConversationQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new ConversationQuery(get_called_class());
    }
}
