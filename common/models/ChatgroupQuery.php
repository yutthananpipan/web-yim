<?php

namespace common\models;

/**
 * This is the ActiveQuery class for [[Chatgroup]].
 *
 * @see Chatgroup
 */
class ChatgroupQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * {@inheritdoc}
     * @return Chatgroup[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * {@inheritdoc}
     * @return Chatgroup|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
