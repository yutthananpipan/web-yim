<?php

namespace backend\controllers;

use common\models\Conversation;
use common\models\Message;
use common\models\User;
use Exception;
use Yii;
use common\models\Chatgroup;
use common\models\ChatgroupSearch;
use yii\base\BaseObject;
use yii\helpers\ArrayHelper;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * ChatgroupController implements the CRUD actions for Chatgroup model.
 */
class ChatgroupController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                    'get-conversation' => ['get'],
                ],
            ],
        ];
    }

    /**
     * Lists all Chatgroup models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new ChatgroupSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Chatgroup model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Chatgroup model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Chatgroup();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Chatgroup model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Chatgroup model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Chatgroup model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Chatgroup the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Chatgroup::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }

    public function getChatGroup($id)
    {
        $model = new Chatgroup();

        try {
            $model = Chatgroup::find()
                ->innerJoin(Conversation::tableName(), 'conversation.chatgroup_id = chatgroup.id')
                ->where(['conversation.user_id' => $id])
                ->all();
        } catch (Exception $exception) {
        }

        return $model;
    }

    private function getMessage($chatGroup)
    {
        $model = new Message();

        try {
            $model = Message::find()
                ->where(['cg_id' => $chatGroup])
                ->all();
        } catch (Exception $exception) {
        }

        return $model;
    }

    /**
     * Updates an existing Chatgroup model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param $id
     * @param User $user
     * @return void
     */
    private function createChatGroup($id)
    {
        try {
            $owner = User::findOne($id);
            $users = ArrayHelper::toArray(User::find()->all());
            foreach ($users as $user) {
                if ($user['id'] != $id) {
                    $userModel = Chatgroup::find()->where(['name' => ($owner->username . ',' . $user['username']),])
                        ->orWhere(['name' => ($user['username'] . ',' . $owner->username),])
                        ->all();

                    if (count($userModel) == 0) {
                        $chatGroup = new Chatgroup();
                        $chatGroup->name = ($owner->username . ',' . $user['username']);
                        $chatGroup->created_by = $id;
                        $chatGroup->created_at = Yii::$app->formatter->asTimestamp(new \DateTime());
                        $chatGroup->updated_by = $id;
                        $chatGroup->updated_at = Yii::$app->formatter->asTimestamp(new \DateTime());
                        $chatGroup->last_message = '';
                        $chatGroup->type = 'INDIVIDUAL';
                        if ($chatGroup->save()) {
                            $conversation1 = new Conversation();
                            $conversation1->user_id = $owner->id;
                            $conversation1->chatgroup_id = $chatGroup->id;
                            $conversation1->save();

                            $conversation2 = new Conversation();
                            $conversation2->user_id = $user['id'];
                            $conversation2->chatgroup_id = $chatGroup->id;
                            $conversation2->save();
                        }


                    }

                }
            }
        } catch (Exception $exception) {
        }

    }

    public function actionCreateConversation($data)
    {
        try {
            $obj = json_decode($data);
            $messages = $obj->messages;
            $chatGroups = $obj->chatGroups;
            foreach ($messages as $msg) {
                try {
                    $message = new Message();
                    $message->id = $msg->id;
                    $message->message = $msg->message;
                    $message->user_id = $msg->user_id + 0;
                    $message->created_at = $msg->created_at + 0;
                    $message->created_by = $msg->created_by + 0;
                    $message->type = $msg->type;
                    $message->cg_id = $msg->cg_id + 0;
                    $message->save();
                } catch (Exception $e) {
                }
            }

            foreach ($chatGroups as $cg) {
                try {
                    $chatGroup = Chatgroup::find()->where(['id' => $cg->id])->one();
                    $chatGroup->id = $cg->id + 0;
                    $chatGroup->name = $cg->name;
                    $chatGroup->created_at = $cg->created_at + 0;
                    $chatGroup->created_by = $cg->created_by + 0;
                    $chatGroup->updated_at = $cg->updated_at + 0;
                    $chatGroup->updated_by = $cg->updated_by + 0;
                    $chatGroup->last_message = $cg->last_message;
                    $chatGroup->type = $cg->type;
                    $chatGroup->save();
                } catch (Exception $exception) {
                }

            }

            return json_encode(['result' => true]);

        } catch (Exception $exception) {
        }

        return json_encode(['result' => false]);
    }

    public function actionGetConversation($id)
    {
        $conversation = new Conversation();
        $chatGroup[] = new Chatgroup;
        $message = new Message();

        try {
            // Todo Init Chat Group
            $this->createChatGroup($id);

            $conversation = Conversation::find()
                ->where(['user_id' => $id])
                ->all();

            $chatGroup = $this->getChatGroup($id);
            $message = $this->getMessage(ArrayHelper::getColumn($chatGroup, 'id'));

        } catch (Exception $exception) {

        }

        return json_encode([
            'conversation' => ArrayHelper::toArray($conversation),
            'chatgroup' => ArrayHelper::toArray($chatGroup),
            'message' => ArrayHelper::toArray($message)
        ]);
    }
}
