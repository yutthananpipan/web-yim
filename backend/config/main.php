<?php
$params = array_merge(
    require __DIR__ . '/../../common/config/params.php',
    require __DIR__ . '/../../common/config/params-local.php',
    require __DIR__ . '/params.php',
    require __DIR__ . '/params-local.php'
);

return [
    'id' => 'app-backend',
    'basePath' => dirname(__DIR__),
    'controllerNamespace' => 'backend\controllers',
    'bootstrap' => ['log'],
    'modules' => [
        'user' => [
            'class' => 'dektrium\user\Module',
            'enableUnconfirmedLogin' => true,
            'enablePasswordRecovery' => false,
            'enableRegistration' => true,
            'confirmWithin' => 21600,
            'cost' => 12,
            'admins' => ['admin', 'sam'],
            'modelMap' => [
                'Profile' => 'common\models\Profile',
                'User' => 'common\models\User',
                'UserSearch' => 'common\models\UserSearch',

                'RecoveryForm' => 'common\models\RecoveryForm',
                'RegistrationForm' => 'common\models\RegistrationForm',
                'ResendForm' => 'common\models\ResendForm',
                'SettingsForm' => 'common\models\SettingsForm',

            ],

            'controllerMap' => [
                'settings' => 'backend\controllers\SettingsController',
                'admin' => 'backend\controllers\AdminController',
                'security' => 'backend\controllers\SecurityController',
                'recovery' => 'backend\controllers\RecoveryController',
                'registration' => 'backend\controllers\RegistrationController'
            ],

        ],
        'admin' => [
            'class' => 'mdm\admin\Module',
            'layout' => 'left-menu',
            'controllerMap' => [
                'assignment' => [
                    'class' => 'mdm\admin\controllers\AssignmentController',
                    'userClassName' => 'common\models\User',
                    //เรียกใช้โมเดล user ของ dektrium
                ]
            ],
        ],
    ],
    'components' => [
        'request' => [
            'csrfParam' => '_csrf-backend',
        ],
        'session' => [
            // this is the name of the session cookie used for login on the backend
            'name' => 'advanced-backend',
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],
        'urlManager' => [
            'class' => 'yii\web\UrlManager',
            // Disable index.php
            'showScriptName' => false,
            // Disable r= routes
            'enablePrettyUrl' => true,
            'rules' => array(
                '<controller:\w+>/<id:\d+>' => '<controller>/view',
                '<controller:\w+>/<action:\w+>/<id:\d+>' => '<controller>/<action>',
                '<controller:\w+>/<action:\w+>' => '<controller>/<action>',
                'module/<module:\w+>/<controller:\w+>/<action:\w+>' => '<module>/<controller>/<action>',
            ),
        ],
    ],
    'as access' => [
        'class' => 'mdm\admin\components\AccessControl',
        'allowActions' => [
            //module, controller, action ที่อนุญาตให้ทำงานโดยไม่ต้องผ่านการตรวจสอบสิทธิ์
            'site/*',
            'user/security/*',
            'user/registration/*',
            'user/settings/*',
            'user/recovery/*',

            'user/admin/login',
            'user/admin/list-member',
            'user/admin/set-profile',

            'chatgroup/get-chatgroup',
            'chatgroup/get-conversation',
            'chatgroup/create-chatgroup',
            'chatgroup/create-conversation',

            'message/get-message',
            'message/create-message',
        ]
    ],
    'params' => $params,
];
