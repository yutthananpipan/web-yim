<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\Chatgroup */

$this->title = 'Update Chatgroup: ' . $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Chatgroups', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="chatgroup-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
