<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\Chatgroup */

$this->title = 'Create Chatgroup';
$this->params['breadcrumbs'][] = ['label' => 'Chatgroups', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="chatgroup-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
